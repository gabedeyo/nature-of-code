// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");

function clear() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

//placeholders for mouse x,y position
var mouseX = 0;
var mouseY = 0;
var mouseDown = false;

canvas.addEventListener('mousedown', setMouseDown, false);
canvas.addEventListener('mouseup', setMouseUp, false);

canvas.onmousemove = function(e) {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

var mouse = new PVector(mouseX, mouseY);

function updateMouse(){
    mouse.x = mouseX;
    mouse.y = mouseY;
}

function setMouseDown() {
    mouseDown = true;
}
function setMouseUp(){
    mouseDown = false;
}

function drawLiquid() {
    context.rect(0, canvas.height/2, canvas.width, canvas.height);
    context.fillStyle = 'blue';
    context.fill();
}

// get 2D context
var context = canvas.getContext('2d');
var tick = 25;

var mover = new Mover(5);

function update() {
    clear();
    drawLiquid();

    mover.setGravity();
    mover.setWind(0.01, 0);
    //if(mover.location.y > canvas.height-50){
        mover.setFriction(-0.01);
    //}
    mover.setDrag(0.1);
    

    mover.drawMover(context);
    mover.checkEdges();
    mover.move();

    
}
var timer = setInterval(update, tick);