// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");

function clear() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

//placeholders for mouse x,y position
var mouseX = 0;
var mouseY = 0;
var mouseDown = false;

canvas.addEventListener('mousedown', setMouseDown, false);
canvas.addEventListener('mouseup', setMouseUp, false);

canvas.onmousemove = function(e) {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

var mouse = new PVector(mouseX, mouseY);

function updateMouse(){
    mouse.x = mouseX;
    mouse.y = mouseY;
}

function setMouseDown() {
    mouseDown = true;
}
function setMouseUp(){
    mouseDown = false;
}

// get 2D context
var context = canvas.getContext('2d');
var tick = 25;

var movers = new Array(100);

for(var i = 0; i < movers.length; i++) {
    movers[i] = new Mover(getRandomArbitrary(0.1, 5), getRandomArbitrary(0 + i, canvas.width - i), 100);
    
    var wind = new PVector(0.01, 0);
    movers[i].applyForce(wind);
}

function update() {
    clear();
    for(var i = 0; i < movers.length; i++) {
        movers[i].drawMover(context);
        movers[i].checkEdges();
        movers[i].move();
    }
}
var timer = setInterval(update, tick);