// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");

function clear() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

//placeholders for mouse x,y position
var mouseX = 0;
var mouseY = 0;
var mouseDown = false;

canvas.addEventListener('mousedown', setMouseDown, false);
canvas.addEventListener('mouseup', setMouseUp, false);

canvas.onmousemove = function(e) {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

var mouse = new PVector(mouseX, mouseY);

function updateMouse(){
    mouse.x = mouseX;
    mouse.y = mouseY;
}

function setMouseDown() {
    mouseDown = true;
}
function setMouseUp(){
    mouseDown = false;
}

// get 2D context
var context = canvas.getContext('2d');
var tick = 25;

//var mover = new Mover(-0.001, 0.01);
var mover = new Mover();
var wind = new PVector(0.001, 0);
var gravity = new PVector(0, 0.01);

mover.applyForce(gravity);

function update() {
    clear();

    if(mouseDown) {
        mover.applyForce(wind);
    } else {
        mover.acceleration.x = 0;
    }

    mover.drawMover(context);
    mover.setBoundaries(5);
    mover.move();
}
var timer = setInterval(update, tick);