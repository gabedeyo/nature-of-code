// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 400;
canvas.height = 400;
canvas.setAttribute("style", "border: 1px solid black;");

// get 2D context
var context = canvas.getContext('2d');

function getRandomArbitrary(min, max){
    return Math.random() * (max - min) + min;
}

function MonteCarlo() {
    while(true){ //Do this until we find a qualifying random value

        var r1 = getRandomArbitrary(0, 1); //Pick random value
        var probability = r1; // Assign a probability

        var r2 = getRandomArbitrary(0, 1); //Pick second random value

        if(r2 < probability) {  // Does it qualify? If so, we're done
            return r1;
        }
    }
}

//placeholders for mouse x,y position
var mouseX = 0;
var mouseY = 0;

canvas.onmousemove = function(e) {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

var tick = 100;



//Random Walker Class
class Walker {
    constructor(width = 50, height = 50){
        this.width = width;
        this.height = height;
        this.x = canvas.width/2;
        this.y = canvas.height/2;
        this.color = "black";
    }
}

//Draw the walker (black square (point))
function drawWalker(walker) {
    context.fillStyle = walker.color;
    context.fillRect(walker.x, walker.y, walker.width, walker.height);
}

//Random Step for the walker
function step(walker) {
    //Set step between -1f and 1f exclusively
    var stepx = getRandomArbitrary(-1, 1);
    var stepy = getRandomArbitrary(-1, 1);
    
    walker.x += stepx;
    walker.y += stepy;
}

//Random Step with priority
function stepWithPriority(walker) {
    var r = Math.random(1);
    if(r < 0.4) {
        walker.x++;
    } else if(r < 0.6) {
        walker.x--;
    } else if(r < 0.8) {
        walker.y++;
    } else {
        walker.y--;
    }
}

function stepWithMousePriority(walker) {
    var stepx = getRandomArbitrary(-1, 1);
    var stepy = getRandomArbitrary(-1, 1);

    var r = Math.random(1);
    if(r < 0.5){
        walker.x < mouseX ? walker.x++ : walker.x--;
        walker.y < mouseY ? walker.y++ : walker.y--;
    } else {
        walker.x += stepx;
        walker.y += stepy;
    }
}

// Standard Normal variate using Box-Muller transform.
function nextGaussian() {
    var u = 1 - Math.random(); // Subtraction to flip [0, 1) to (0, 1].
    var v = 1 - Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

// Use a Gaussian Distribution to determine step size
function stepWithGaussian(walker) {
    var num1 = nextGaussian();
    var num2 = nextGaussian();
    var standard_deviation = 3;
    var mean = 0;

    var result1 = standard_deviation * num1 + mean;
    var result2 = standard_deviation * num2 + mean;

    walker.x += result1;
    walker.y += result2;
}

// Avoid oversampling using a custom probability distribution called a Levy flight
function stepWithLevyFlight(walker) {
    var r = getRandomArbitrary(0, 1);

    var xstep;
    var ystep;

    if(r < 0.01) { // A 1% chance of taking a large step    
        xstep = getRandomArbitrary(-100, 100);
        ystep = getRandomArbitrary(-100, 100);
    } else {
        xstep = getRandomArbitrary(-1, 1);
        ystep = getRandomArbitrary(-1, 1);
    }

    walker.x += xstep;
    walker.y += ystep;
}

function stepWithMonteCarlo(walker) {
    var stepSize = getRandomArbitrary(0, 10); // A uniform distribution of step sizes. Change this!
    console.log(stepSize);

    var stepx = getRandomArbitrary(-stepSize, stepSize);
    var stepy = getRandomArbitrary(-stepSize, stepSize);

    walker.x += stepx;
    walker.y += stepy;
}

var walker = new Walker(1, 1);

//Draw function
function draw(){
    drawWalker(walker);
    //step(walker);
    //stepWithPriority(walker);
    //stepWithMousePriority(walker);
    //stepWithGaussian(walker);
    //stepWithLevyFlight(walker);
    stepWithMonteCarlo(walker);
}
var myTimer=setInterval(draw, tick);