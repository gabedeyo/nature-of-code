// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");


var seed = 1;
function random() {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}

function map_range(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

// get 2D context
var context = canvas.getContext('2d');

//var generator = new Noise_1D();

noise.seed(Math.random());
var xoff = 0.0;

var tick = 5;

// for(var x = 0; x < canvas.width; x++) {
//     var yoff = 0.0;

//     for(var y = 0; y < canvas.height; y++) {
//         var brightness = noise.simplex2(xoff, yoff);
//         context.globalAlpha = brightness;
//         context.fillRect(x, y, 1, 1);

//         yoff += 0.01;
//     }
//     xoff += 0.01;
// }

function step(){

    context.clearRect(0, 0, canvas.width, canvas.height);

    for(var x = 0; x < canvas.width; x++) {
        var yoff = 0.0;

        for(var y = 0; y < canvas.height; y++) {
            var brightness = noise.perlin3(xoff, yoff, tick);
            context.globalAlpha = brightness;
            context.fillRect(x, y, 1, 1);

            yoff += 0.01;
        }
        xoff += 0.01;
    }
}

var timer = setInterval(step, tick);