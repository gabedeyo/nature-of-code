// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");


var seed = 1;
function random() {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}


function map_range(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}



// get 2D context
var context = canvas.getContext('2d');

var tick = 15;

// Walker Class
class Walker {
    constructor(){
        this.x = canvas.width * 0.5;
        this.y = canvas.height * 0.5;

        this.tx = random();
        this.ty = random();

        this.radius = 15;
    }
}

//Draw the walker (black square (point))
function drawWalker(x, y, radius, color) {
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    context.arc(x, y, radius, 0, 2 * Math.PI, false);

    context.fillStyle = color;
    context.fill();
    context.lineWidth = 1;
    context.strokeStyle = "black";
    context.stroke();
}

var walker = new Walker();
var generator = new Noise_1D();

function step(){
    walker.x = map_range(generator.noise(walker.tx), 0, 1, 0 + walker.radius, canvas.width - walker.radius);
    walker.y = map_range(generator.noise(walker.ty), 0, 1, 0 + walker.radius, canvas.height - walker.radius);

    walker.tx+= 0.01;   
    walker.ty+= 0.011;
}

//Draw function
function draw(){
    step();
    drawWalker(walker.x, walker.y, walker.radius, "grey");
}
var myTimer=setInterval(draw, tick);