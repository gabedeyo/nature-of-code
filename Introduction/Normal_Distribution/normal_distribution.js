// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 400;
canvas.height = 400;
canvas.setAttribute("style", "border: 1px solid black;");

// get 2D context
var context = canvas.getContext('2d');

context.globalAlpha = 0.2;

// Standard Normal variate using Box-Muller transform.
function nextGaussian() {
    var u = 1 - Math.random(); // Subtraction to flip [0, 1) to (0, 1].
    var v = 1 - Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

//Draw function
function draw(){
    var num = nextGaussian();
    var standard_deviation = 255;
    var mean = 85;

    var result = standard_deviation * num + mean;

    context.fillStyle = 'rgb('+result+','+result+','+result+')';

    context.fillRect(nextGaussian()*50 + (canvas.width*0.5), nextGaussian()*50 + (canvas.height*0.5), 10, 10);
}
var timer = setInterval(draw, 50);