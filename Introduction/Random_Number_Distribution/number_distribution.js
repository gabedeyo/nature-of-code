// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 400;
canvas.height = 400;
canvas.setAttribute("style", "border: 1px solid black;");

// get 2D context
var context = canvas.getContext('2d');
// bar color
context.fillStyle = "grey";

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

var randomCounts = new Array(20).fill(0);

//Random Number Distribution Bar
function drawBar() {
    var index = getRandomInt(0, randomCounts.length);
    randomCounts[index]++;

    var w = canvas.width/randomCounts.length;

    for(var x = 0; x < randomCounts.length; x++) {
        context.fillRect(x*w, canvas.height - randomCounts[x], w-1, randomCounts[x]);
    }
}

//Draw function
function draw(){
    drawBar();
}
var myTimer=setInterval(draw, 10);