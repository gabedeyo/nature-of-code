// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");

// get 2D context
var context = canvas.getContext('2d');
var tick = 10;

class Ball{
    constructor(xVelocity, yVelocity){
        this.location = new PVector(canvas.width*0.5, canvas.height*0.5);
        this.velocity = new PVector(xVelocity, yVelocity);

        this.radius = 25;
        this.color = 'grey';
    }
}

function drawBall(ball) {
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    context.arc(ball.location.x, ball.location.y, ball.radius, 0, 2 * Math.PI, false);
    context.fillStyle = ball.color;
    context.fill();

    context.lineWidth = 1;
    context.strokeStyle = "black";
    context.stroke();
}

var ball = new Ball(1, 3.3);

function draw() {
    drawBall(ball);

    ball.location.add(ball.velocity);

    if((ball.location.x > canvas.width - ball.radius) || (ball.location.x < 0 + ball.radius)) {
        ball.velocity.x *= -1;
    }
    if((ball.location.y > canvas.height - ball.radius) || (ball.location.y < 0 + ball.radius)) {
        ball.velocity.y *= -1;
    }
}
var timer = setInterval(draw, tick);