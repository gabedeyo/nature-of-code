// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");

// get 2D context
var context = canvas.getContext('2d');
var tick = 10;

//placeholders for mouse x,y position
var mouseX = 0;
var mouseY = 0;

canvas.onmousemove = function(e) {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

function drawLine(start, finish){
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    context.moveTo(start.x, start.y);
    context.lineTo(finish.x, finish.y);
    context.stroke();
}

var mouse = new PVector(mouseX, mouseY);
var center = new PVector(canvas.width*0.5, canvas.height*0.5);
var zero = new PVector(0, 0);

function draw() {
    mouse.x = mouseX;
    mouse.y = mouseY;
    
    mouse.normalize();
    mouse.mult(100);

    drawLine(zero, mouse);
}
var timer = setInterval(draw, tick);