// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");

// get 2D context
var context = canvas.getContext('2d');
var tick = 10;

var mover = new Mover();
var topspeed = 10;

function doKeyDown(evt) {
    switch(evt.keyCode) {
        case 38: /* up arrow pressed */
            mover.acceleration.x = 0.01;
            break;
        case 40: /* down arrow pressed */
            mover.acceleration.x = -0.01;
            break;
    }
}

window.addEventListener('keydown', doKeyDown, true);

function update() {
    mover.drawVehicle(context);
    mover.velocity.add(mover.acceleration);
    mover.location.add(mover.velocity);

    mover.velocity.limitMax(topspeed);
    mover.velocity.limitMin(0);

    mover.checkEdges();

    
}
var timer = setInterval(update, tick);