// create a canvas to work with
var canvas = document.createElement('canvas');
document.body.appendChild(canvas);

// style canvas
canvas.id     = "canvas";
canvas.width  = 500;
canvas.height = 500;
canvas.setAttribute("style", "border: 1px solid black;");

function clear() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

//placeholders for mouse x,y position
var mouseX = 0;
var mouseY = 0;

canvas.onmousemove = function(e) {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

var mouse = new PVector(mouseX, mouseY);

function updateMouse(){
    mouse.x = mouseX;
    mouse.y = mouseY;
}

// get 2D context
var context = canvas.getContext('2d');
var tick = 25;

//var mover = new Mover(-0.001, 0.01);
var movers = new Array(10);
for(var i = 0; i < movers.length; i++) {
    movers[i] = new Mover();
}
var topspeed = 5;

function update() {
    updateMouse();
    clear();

    for(var i = 0; i < movers.length; i++) {
        movers[i].drawMover(context);
        movers[i].setBoundaries(5);

        movers[i].moveTowardMouse(mouse);
    }
}
var timer = setInterval(update, tick);