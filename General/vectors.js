class PVector {
    constructor(x, y){
        this.x = x;
        this.y = y;
    }

    limit(n) {
        this.limitMax(n);
        this.limitMin(-n);
    }

    limitMax(max) {
        if(this.x > max) {
            this.x = max;
        }
        if(this.y > max) {
            this.y = max;
        }
    }

    limitMin(min) {
        if(this.x < min) {
            this.x = min;
        }
        if(this.y < min) {
            this.y = min;
        }
    }

    random2D() {
        var tempVec = new PVector(getRandomArbitrary(-1, 1), getRandomArbitrary(-1, 1));
        tempVec.normalize();
        return tempVec;
    }

    add(PVector) {
        this.x += PVector.x;
        this.y += PVector.y;
    }
    
    static add(Vector1, Vector2) {
        return new PVector(Vector1.x + Vector2.x, Vector1.y + Vector2.y);
    }

    sub(PVector) {
        this.x -= PVector.x;
        this.y -= PVector.y;
    }

    static sub(Vector1, Vector2) {
        return new PVector(Vector1.x - Vector2.x, Vector1.y - Vector2.y);
    }

    mult(n) {
        this.x *= n;
        this.y *= n;
    }

    static mult(Vector, n) {
        return new PVector(Vector.x * n, Vector.y * n);
    }

    div(n) {
        this.x /= n;
        this.y /= n;
    }
    
    static div(Vector, n) {
        return new PVector(Vector.x / n, Vector.y / n);
    }

    mag() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    normalize() {
        var m = this.mag();
        if(m != 0) {
            this.div(m);
        }
    }
}