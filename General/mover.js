var gravity = new PVector(0, 0.1);

class Mover{
        constructor(m){
                this.size = 100;
                this.color = 'grey';

                this.mass = m;
                this.radius = m * 5;

                this.location = new PVector(getRandomArbitrary(0 + this.radius, canvas.width - this.radius), getRandomArbitrary(0 + this.radius, canvas.height - this.radius));
                //this.location = new PVector(x, y);
                this.velocity = new PVector(0, 0);
                this.acceleration = new PVector(0, 0);

                //this.applyForce(new PVector(gravity.x, gravity.y * this.mass));
        }

        //Draw a grey ellipse
        drawMover(context) {                
                context.beginPath();
                context.arc(this.location.x, this.location.y, this.radius, 0, 2 * Math.PI, false);
                context.fillStyle = this.color;
                context.fill();
        
                context.lineWidth = 1;
                context.strokeStyle = "black";
                context.stroke();
        }

        //Draw car-ish looking shapes
        drawVehicle(context) {
                context.clearRect(0, 0, canvas.width, canvas.height);
                
                context.rect(this.location.x-this.size, this.location.y-(this.size * 0.75), this.size, this.size *0.75);
                context.stroke();

                context.beginPath();
                context.arc(this.location.x, this.location.y, this.radius *0.75, 0, 2 * Math.PI, false);
                context.fillStyle = 'black';
                context.fill();
                context.stroke();

                context.beginPath();
                context.arc(this.location.x - this.size, this.location.y, this.radius *0.75, 0, 2 * Math.PI, false);
                context.fillStyle = 'black';
                context.fill();
                context.stroke();
        }

        //Check for edge collision
        checkEdges() {
                //Use bounce or transport
                this.bounce();
        }

        //Bounce away from walls
        bounce() {
                // if((this.location.x > canvas.width - this.radius) || (this.location.x < 0 + this.radius)) {
                //         this.velocity.x *= -1;
                // }
                // if((this.location.y > canvas.height - this.radius) || (this.location.y < 0 + this.radius)) {
                //         this.velocity.y *= -1;
                // }

                if(this.location.x >= canvas.width - this.radius) {
                        this.location.x -= 1;
                        this.velocity.x *= -1;
                } else if (this.location.x < 0 + this.radius) {
                        this.location.x += 1;
                        this.velocity.x *= -1;
                }

                if(this.location.y > canvas.height - this.radius) {
                        this.location.y -= 1;
                        this.velocity.y *= -1;
                } else if(this.location.y < 0 + this.radius) {
                        this.location.y += 1;
                        this.velocity.y *= -1;
                }
        }

        setFriction(c) {
                var friction = new PVector(this.velocity.x, this.velocity.y);

                friction.mult(-1);
                friction.normalize();
                friction.mult(c);

                this.applyForce(friction);
        }

        setDrag(c) {
                var speed = this.velocity.mag();
                var dragMag = c * speed + speed;

                var drag = new PVector(this.velocity.x, this.velocity.y);
                drag.mult(-1);
                drag.normalize();
                drag.mult(dragMag);
        }

        setWind(x, y) {
                this.applyForce(new PVector(x, y));
        }

        setGravity() {
                this.applyForce(new PVector(gravity.x, gravity.y * this.mass));
        }

        //Transport to opposite collision point
        transport() {
                if(this.location.x > canvas.width) {
                        this.location.x = 0;
                } else if (this.location.x < 0) {
                        this.location.x = canvas.width;
                }

                if(this.location.y > canvas.height) {
                        this.location.y = 0;
                } else if(this.location.y < 0) {
                        this.location.y = canvas.height;
                }
        }

        //Limit speed of the mover
        setSpeedLimit(speed) {
                //this.velocity.limit(speed);
                this.acceleration.limitMax(speed);
        }

        applyForce(force) {
                var f = PVector.div(force, this.mass);
                this.acceleration.add(f);
        }

        move() {
                this.velocity.add(this.acceleration);
                this.location.add(this.velocity);
                this.acceleration.mult(0);
        }

        randomMove() {
                this.acceleration = this.acceleration.random2D();

                this.velocity.add(this.acceleration);
                this.location.add(this.velocity);
        }

        moveTowardMouse(mouse) {
                var dir = PVector.sub(mouse, this.location);
                var temp = dir.mag();
                dir.normalize();

                dir.mult(temp * 0.005);

                this.acceleration = dir;

                this.velocity.add(this.acceleration);
                this.location.add(this.velocity);
        }
}

