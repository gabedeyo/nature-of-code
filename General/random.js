/*
    Number between 0 (inclusive) and 1 (exclusive)
*/
function getRandom() {
    return Math.random();
}

/*
    Number between two values
*/
function getRandomArbitrary(min, max){
    return Math.random() * (max - min) + min;
}

/*
    Integer between two values
*/
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

/*
    Integer between two values, inclusive
*/
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
}

/*
    Seeded Random
*/
var seed = 1;
function random() {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}