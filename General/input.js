
/* Keyboard Input */
function doKeyDown(evt) {
    switch(evt.keyCode) {
        case 38: /* up arrow pressed */
            break;
        case 40: /* down arrow pressed */
            break;
        case 37: /* left arrow pressed */
            break;
        case 39: /* right arrow pressed */
            break;

    }
}

window.addEventListener('keydown', doKeyDown, true);

/* Mouse Input */
var mouseX = 0;
var mouseY = 0;
var mouseDown = false;

var mouse = new PVector(mouseX, mouseY);

canvas.addEventListener('mousedown', setMouseDown, false);
canvas.addEventListener('mouseup', setMouseUp, false);

canvas.onmousemove = function(e) {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
}

//Call in update
function updateMouse(){
    mouse.x = mouseX;
    mouse.y = mouseY;
}

function setMouseDown() {
    mouseDown = true;
}
function setMouseUp(){
    mouseDown = false;
}